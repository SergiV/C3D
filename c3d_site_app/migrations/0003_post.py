# Generated by Django 2.1 on 2018-08-03 10:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('c3d_site_app', '0002_auto_20180803_1104'),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author', models.CharField(max_length=100)),
                ('title', models.CharField(max_length=100)),
                ('text', models.TextField()),
            ],
        ),
    ]
