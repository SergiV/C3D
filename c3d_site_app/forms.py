# В нем нужно написать:
from django import forms
from .models import Post
from .models import Categories

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('author', 'title', 'text', 'rate', 'published_date')
# поля pub_date и id заполняются сами