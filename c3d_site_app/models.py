from django.db import models
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.contenttypes.fields import GenericRelation

class Categories(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()
    def __str__(self):
        return self.title
    def __repr__(self):
        return self.title


class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=150)
    text = models.TextField()
    category = GenericRelation(Categories)
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)
    rate = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)], default=0)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title