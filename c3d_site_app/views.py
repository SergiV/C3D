from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone
from .models import Post
from .models import Categories
from .forms import PostForm
from django.shortcuts import redirect


# def index(request):
#     return HttpResponse("Любой текст")

def index(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    star = ''
    return render(request, 'c3d_site_app/index.html', {'posts': posts})

def post(request, p_id):
    f = Post.objects.get(id=p_id)
    return render(request, 'c3d_site_app/post.html', {'post_id': f, 'text': f.text})


def category(request, category):
    return render(request, "c3d_site_app/category.html", {"category": Categories.objects.get(title=category)})


def categories(request):
    return render(request, "c3d_site_app/categories.html", {"categories": Categories.objects.all})


def new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            add_post = form.save()
            return redirect('/')
    else:
        return render(request, 'c3d_site_app/new.html', {'new_post_form': PostForm()})