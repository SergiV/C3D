from django.conf.urls import url


from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^post/(\d+)/', views.post, name='post'),
    url('^category/(?P<category>[A-Za-z0-9]+)/$', views.category, name='category'),
    url(r'^categories/', views.categories, name='categories+'),
    url(r'^new/', views.new, name='name'),
]

