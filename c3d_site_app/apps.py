from django.apps import AppConfig


class C3DSiteAppConfig(AppConfig):
    name = 'c3d_site_app'
