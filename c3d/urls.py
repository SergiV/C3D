"""c3d URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
# from django.urls import path
# from django.conf.urls import include, url
#
# from accounts.views import ELoginView    # Представление для авторизации из модуля accounts
#
# urlpatterns = [
#     url(r'^$', include('c3d_site_app.urls')),
#     url(r'^admin/login/', ELoginView.as_view()),
#     url(r'^admin/', admin.site.urls),
#     url(r'^accounts/', include('accounts.urls')),    # также добавим url модуля авторизаций
# ]

from django.conf.urls import url, include
from django.contrib import admin

from accounts.views import ELoginView  # Представление для авторизации из модуля accounts

# Чтобы перехватить страницу авторизации, необходимо
# прописать путь к этой странице перед url админ-панели
# и указать представление, которое будет теперь обрабатывать авторизацию
urlpatterns = [
    url(r'^admin/login/', ELoginView.as_view()),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('accounts.urls')),  # также добавим url модуля авторизаций
    url(r'', include('c3d_site_app.urls')),
    ]